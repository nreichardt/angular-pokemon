# Pokemon Catalogue

Pokemon Catalogue App made with Angular

The project had 3 page requirements:

### 1. Landing page:

- User must be able to enter their trainer name.

### 2. Catalogue Page:

- User must be displayed a list of pokemon, in 'card' style. Pokemon name and image should be displayed.
- Each card must be clickable and take the user to a detail page.
- User may not have access to this page unless they have entered a trainer name.

### 3. Profile page:

- The profile page must display all the pokemon the user has 'collected'.
- Each pokemon must be clickable and take the user to the detail page.

### 4. Pokemon Detail Page:

- The pokemon detail page must display the chosen pokemons information and stats.
- There must be a button to add the pokemon, to the users collection.

### Min. Requirements:

- Use the latest Angular with the Angular CLI.
- Use Components to:
  - Create 'Root' or 'Parent' components for pages.
  - Create reusable pieces of UI.
- Use the Angular Router to:
  - Selectively display 'parent' components based on the URL's path.
- Use the Angular Guard pattern to:
  - Restrict access to pages.
- Use Services to:
  - Share data between Components.
  - Make HTTP Requests using the HttpClient.
