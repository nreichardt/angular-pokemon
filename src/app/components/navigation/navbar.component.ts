import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  constructor(private readonly auth: AuthService) {}

  get currentUser(): string {
    return this.auth.currentUser();
  }

  handleLogout() {
    this.auth.logout();
  }
}
