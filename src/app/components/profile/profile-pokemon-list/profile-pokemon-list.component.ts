import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileStorageService } from 'src/app/services/profileStorage.service';
import { Pokemon } from '../../../models/pokemon.model';
import { PokemonDetailService } from '../../../services/pokemon-detail.service';

@Component({
  selector: 'app-profile-pokemon-list',
  templateUrl: './profile-pokemon-list.component.html',
  styleUrls: ['./profile-pokemon-list.component.css'],
})
export class ProfilePokemonListComponent implements OnInit {
  constructor(
    private readonly pokemonDetailService: PokemonDetailService,
    private readonly profileStorageService: ProfileStorageService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.profileStorageService.setPokemonArray();
  }
  page = 1;
  pageSize = 12;

  get pokemonArray(): Pokemon[] {
    return this.profileStorageService.pokemonArray();
  }

  handlePokemonClicked(pokemon: Pokemon) {
    this.pokemonDetailService.setPokemon(pokemon);
    this.router.navigate(['/detail']);
  }
}
