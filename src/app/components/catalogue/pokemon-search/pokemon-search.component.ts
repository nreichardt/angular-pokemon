import { Component, EventEmitter } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-search',
  templateUrl: './pokemon-search.component.html',
  styleUrls: ['./pokemon-search.component.css'],
})
export class PokemonSearchComponent {
  constructor(private readonly pokemonService: PokemonService) {}

  searchString: string = '';

  public onSearchChange(searchValue: any) {
    this.searchString = searchValue.value;
    this.pokemonService.updateDisplayedPokemon(this.searchString.toLowerCase());
  }
}
