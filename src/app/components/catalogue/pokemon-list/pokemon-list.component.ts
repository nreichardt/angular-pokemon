import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaginationService } from 'src/app/services/pagination.service';
import { Pokemon } from '../../../models/pokemon.model';
import { AuthService } from '../../../services/auth.service';
import { PokemonDetailService } from '../../../services/pokemon-detail.service';
import { PokemonService } from '../../../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css'],
})
export class PokemonListComponent implements OnInit {
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly pokemonDetailService: PokemonDetailService,
    private readonly router: Router,
    private readonly paginationService: PaginationService
  ) {}

  isLoading: boolean = true;

  ngOnInit(): void {
    this.pokemonService.fetchPokemon();
    this.isLoading = false;
  }
  page: number = this.paginationService.currentPage();
  pageSize = 16;

  get displayedPokemon(): Pokemon[] {
    return this.pokemonService.displayedPokemon();
  }
  handlePokemonClicked(pokemon: Pokemon) {
    this.paginationService.setPage(this.page);
    this.pokemonDetailService.setPokemon(pokemon);
    this.router.navigate(['/detail']);
  }
}
