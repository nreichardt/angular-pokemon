import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-landing-page-component',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements OnInit {
  constructor(
    private readonly auth: AuthService,
    private readonly router: Router
  ) {}

  public onSubmit(createForm: NgForm): void {
    if (createForm.valid) {
      this.auth.setCurrentUser(createForm.value.trainerName);
    }
  }

  ngOnInit(): void {
    if (this.auth.currentUser !== undefined) {
      this.router.navigate(['/catalogue']);
    }
  }
}
