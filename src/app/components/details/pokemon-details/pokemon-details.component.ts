import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DetailedPokemon } from 'src/app/models/detailedPokemon.model';
import { AuthService } from 'src/app/services/auth.service';
import { PokemonDetailService } from 'src/app/services/pokemon-detail.service';
import { ProfileStorageService } from 'src/app/services/profileStorage.service';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.css'],
})
export class PokemonDetailsComponent implements OnInit {
  isSaved: boolean = false;
  isMovesShown: boolean = false;
  constructor(
    private readonly pokemonDetailService: PokemonDetailService,
    private readonly auth: AuthService,
    private readonly router: Router,
    private readonly profileStorageService: ProfileStorageService
  ) {}

  ngOnInit(): void {
    if (this.pokemonDetailService.pokemon() === undefined) {
      this.router.navigate(['/catalogue']);
    }
    this.pokemonDetailService.fetchPokemonById();
    this.profileStorageService.setPokemonArray();
    this.checkIsSaved();
  }

  checkIsSaved(): void {
    if (this.profileStorageService.checkIfUserHasPokemon() === true) {
      for (let pokemon of this.profileStorageService.pokemonArray()) {
        if (pokemon.name === this.pokemonDetailService.pokemon()?.name) {
          this.isSaved = true;
        }
      }
    }
  }

  toggleIsMovesShown(): void {
    this.isMovesShown = !this.isMovesShown;
  }

  get isLoading() {
    return this.pokemonDetailService.isLoading();
  }
  get detailedPokemon() {
    return this.pokemonDetailService.detailedPokemon();
  }

  public addPokemonToCollectionClick() {
    this.profileStorageService.storePokemon();
    this.checkIsSaved();
  }

  public handleReturnClick() {
    this.router.navigate(['/catalogue']);
  }
}
