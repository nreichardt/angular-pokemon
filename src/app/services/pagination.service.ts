import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PaginationService {
  private _currentPage: number = 1;

  public currentPage(): number {
    return this._currentPage;
  }
  public setPage(page: number): void {
    this._currentPage = page;
  }
}
