import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private readonly router: Router
  ) {}

  public setCurrentUser(user: string) {
    this.storage.set('auth', user);
    this.router.navigate(['/catalogue']);
  }

  public logout() {
    this.storage.set('auth', undefined);
    this.router.navigate(['/landing']);
  }

  public currentUser(): string {
    return this.storage.get('auth');
  }
}
