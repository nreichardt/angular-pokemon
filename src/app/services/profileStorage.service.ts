import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Pokemon } from '../models/pokemon.model';
import { AuthService } from './auth.service';
import { PokemonDetailService } from './pokemon-detail.service';

@Injectable({
  providedIn: 'root',
})
export class ProfileStorageService {
  private _pokemonArray: Pokemon[] | any = [];

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private readonly auth: AuthService,
    private readonly pokemonDetailService: PokemonDetailService
  ) {}

  public checkIfUserHasPokemon(): boolean {
    if (this.storage.get(this.auth.currentUser()) === undefined) {
      return false;
    } else {
      return true;
    }
  }

  public storePokemon(): void {
    if (this.checkIfUserHasPokemon()) {
      this._pokemonArray = this.storage.get(this.auth.currentUser());
    }
    this._pokemonArray.push(this.pokemonDetailService.pokemon());
    this.storage.set(this.auth.currentUser(), this._pokemonArray);
  }

  public setPokemonArray(): void {
    if (this.storage.get(this.auth.currentUser()) !== undefined) {
      this._pokemonArray = this.storage.get(this.auth.currentUser());
    }
  }

  public pokemonArray(): Pokemon[] {
    return this._pokemonArray;
  }
}
