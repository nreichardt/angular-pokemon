import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { DetailedPokemon } from '../models/detailedPokemon.model';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PokemonDetailService {
  private _pokemon: Pokemon | undefined;
  private _detailedPokemon: DetailedPokemon | undefined;
  private _error: string = '';
  private _isLoading: boolean = false;
  constructor(private readonly http: HttpClient) {}

  fetchPokemonById(): void {
    this._isLoading = true;
    this.http
      .get<DetailedPokemon>(
        `https://pokeapi.co/api/v2/pokemon/${this._pokemon?.id}`
      )
      .pipe(
        tap((responseData: DetailedPokemon) => {
          return {
            ...responseData,
            imgUrl: this._pokemon?.imgUrl,
            fallbackImgUrl: this._pokemon?.fallbackImgUrl,
          };
        })
      )
      .subscribe(
        (detailedPokemon) => {
          this._detailedPokemon = detailedPokemon;
          this._isLoading = false;
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
          this._isLoading = false;
        }
      );
  }

  public pokemon(): Pokemon | undefined {
    return this._pokemon;
  }

  public detailedPokemon(): DetailedPokemon | undefined {
    return this._detailedPokemon;
  }

  public isLoading() {
    return this._isLoading;
  }

  public setPokemon(pokemon: Pokemon): void {
    this._pokemon = pokemon;
  }
}
