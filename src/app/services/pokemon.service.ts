import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemon: Pokemon[] = [];
  private _displayedPokemon: Pokemon[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  fetchPokemon(): void {
    this.http
      .get<any>('https://pokeapi.co/api/v2/pokemon?limit=1118')
      .pipe(
        map((responseData) => {
          const pokemonArray = [];
          for (const pokemon in responseData.results) {
            const pokemonId = responseData.results[pokemon].url.slice(
              34,
              responseData.results[pokemon].url.lastIndexOf('/')
            );
            pokemonArray.push({
              id: pokemonId,
              name: responseData.results[pokemon].name,
              url: responseData.results[pokemon].url,
              imgUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemonId}.svg`,
              fallbackImgUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemonId}.png`,
            });
          }
          return pokemonArray;
        })
      )
      .subscribe(
        (pokemon: Pokemon[]) => {
          this._pokemon = pokemon;
          this._displayedPokemon = pokemon;
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }

  public pokemon(): Pokemon[] {
    return this._pokemon;
  }

  public displayedPokemon(): Pokemon[] {
    return this._displayedPokemon;
  }

  public updateDisplayedPokemon(searchString: string): void {
    this._displayedPokemon = this._pokemon.filter((x) =>
      x.name.includes(searchString)
    );
  }

  public error(): string {
    return this._error;
  }
}
