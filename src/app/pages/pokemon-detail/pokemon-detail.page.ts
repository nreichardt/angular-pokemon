import { Component } from '@angular/core';

@Component({
  selector: 'app-pokemon-detail-page',
  templateUrl: './pokemon-detail.page.html',
  styleUrls: ['./pokemon-detail.page.css'],
})
export class PokemonDetailPage {}
