import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing-module';

import { ImagePreloadDirective } from './directives/image-preload.directive';

import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';
import { PokemonListComponent } from './components/catalogue/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/catalogue/pokemon-list-item/pokemon-list-item.component';
import { PokemonDetailsComponent } from './components/details/pokemon-details/pokemon-details.component';
import { ProfilePokemonListComponent } from './components/profile/profile-pokemon-list/profile-pokemon-list.component';
import { NavbarComponent } from './components/navigation/navbar.component';

import { LandingPage } from './pages/landing/landing.page';
import { PokemonDetailPage } from './pages/pokemon-detail/pokemon-detail.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { ProfilePage } from './pages/profile/profile.page';
import { PokemonSearchComponent } from './components/catalogue/pokemon-search/pokemon-search.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    PokemonSearchComponent,
    ImagePreloadDirective,
    LandingPage,
    PokemonCataloguePage,
    PokemonDetailPage,
    PokemonDetailsComponent,
    ProfilePokemonListComponent,
    ProfilePage,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
