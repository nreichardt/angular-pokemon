export interface DetailedPokemon {
  id: string;
  name: string;
  imgUrl?: string;
  fallbackImgUrl?: string;
  base_experience?: string;
  height?: string;
  moves?: PokemonMoves[];
  types?: PokemonTypes[];
  stats?: PokemonStats[];
  abilities?: PokemonAbilities[];
  weight?: string;
}

interface PokemonMoves {
  move: PokemonMove;
}
interface PokemonMove {
  name: string;
}

interface PokemonTypes {
  type: PokemonType;
}
interface PokemonType {
  name: string;
}

interface PokemonStats {
  base_stat: string;
  stat: PokemonStat;
}
interface PokemonStat {
  name: string;
}

interface PokemonAbilities {
  ability: PokemonAbility;
}
interface PokemonAbility {
  name: string;
}
