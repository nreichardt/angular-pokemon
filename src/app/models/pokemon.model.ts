export interface Pokemon {
  id: string;
  name: string;
  url: string;
  imgUrl: string;
  fallbackImgUrl: string;
}
